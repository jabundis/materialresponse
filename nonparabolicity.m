function beta0 = nonparabolicity()

%All in SI units
hbar = 1.0545718e-34;
me = 9.10938356e-31;
qe = 1.6021766208e-19;


% E0[eV], Delta0[eV], E0p[eV], Delta0p[eV], EP[eV], Pprime[eVcm], Q[eVcm], gamma2, gamma3
GaAs = [1.519, 0.341, 4.488, 0.171, 28.8, 4.780e-8, 8.165e-8, 2.06, 2.93];
InAs = [0.417, 0.390, 4.390, 0.240, 21.5, 0.660e-8, 7.720e-8, 8.50, 9.20];
AlAs = [3.099, 0.280, 4.540, 0.150, 21.1, 0.340e-8, 8.070e-8, 0.82, 1.42];  

%Bowing
InGaAsB = [0.477, 0.15, 0, 0, -1.48, 0, 0, 0, 0.481];
AlInAsB = [0.700, 0.15, 0, 0, -4.81, 0, 0, 0, 0];

%Interpolation In(0.53)Ga(0.47)As and Al(0.48)In(0.52)As
x = 0.53;
InGaAs = x*InAs + (1-x)*GaAs - x*(1-x)*InGaAsB;
y = 0.48;
AlInAs = y*AlAs + (1-y)*InAs - y*(1-y)*AlInAsB;

%Interpolation Al(x)Ga(1-x)As
x = 0.30;
AlGaAsB = [-0.127+1.310*x, 0, 0, 0, 0, 0, 0, 0, 0];
AlGaAs = x*AlAs + (1-x)*GaAs - x*(1-x)*AlGaAsB;

%suggestions for In(0.53)Ga(0.47)As
%E0=0.816; gamma2 = 4.18; gamma3 = 4.84; P = sqrt( k * 25.3 ) %[eVcm]

%suggestions for Al(0.48)In(0.52)As
%P = sqrt( k * 22.5 )

materials = [AlInAs; InGaAs; AlGaAs; GaAs];
n = size(materials, 1);
beta0 = zeros(1,n);  

for i = 1:n
    E0=materials(i,1);Delta0=materials(i,2);E0p=materials(i,3);Delta0p=materials(i,4);EP=materials(i,5);
    Pprime=materials(i,6);Q=materials(i,7);gamma2=materials(i,8);gamma3=materials(i,9);

%    k = (1.3123e15)^-1 / 2; P=sqrt( k * EP);  %P in [eVcm]
    k =  hbar^2 / (2*me) /qe * 1e4; P=sqrt( k * EP);  %P in [eVcm]

    %Transform to SI Units
    P=P*qe/100; E0=E0*qe; Delta0=Delta0*qe;
    Q=Q*qe/100; Pprime=Pprime*qe/100; Delta0p=Delta0p*qe;E0p=E0p*qe;

    gamma2=gamma2+(1/3)*(me/hbar^2)*Q^2/E0p;
    gamma3=gamma3-(1/3)*(me/hbar^2)*Q^2/E0p;


    k1=me / hbar^2;
    k2 = 1 / hbar;
    k3 = hbar / me;
    k4 = hbar^4/me^3;

    ps=P*k1; eg=E0*k2; lambda=Delta0*k2; qs=Q*k1;
    psP=Pprime*k1; lambdaP=Delta0p*k2;egP=E0p*k2;

    del1_10 = (1/3)*ps^2 * (1/eg^2 + 2/(eg*(eg + lambda)))*(gamma2-gamma3);
    del1_11 = (2/9)*(ps*qs)^2 * ( 2/(eg*(eg+lambda)*(eg-egP-lambdaP)) + 1/(eg^2*(eg-egP)) );
    del1_12 = (2/9)*(psP*qs)^2 * ( 2/(eg*(eg-egP)*(eg-egP-lambdaP)) + 1/((eg+lambda)*(eg-egP-lambdaP)^2) );

    beta0(i) = 6*k4*(del1_10 + k3*(del1_11 + del1_12)); %[J*m^4]
    beta0(i) = beta0(i) / qe *1e8;  %now in units [eVcm^4]
end
