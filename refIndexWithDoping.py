import numpy as np
import parametersAdachi as PA
import os

currentDir = os.path.dirname( __file__ )

def epsFromRefInd(nList):
    """
    Returns dielectric constants for the complex refractive indices
    in "nList". (It's assumed a relative permeability of one
                 and planewave of form exp(-iwt), i.e, eps =
                 eps1 + i*eps2)

    Input:
        nList (array-like) --> refractive indices
            nList[i]->float or complex

    Returns:
        eps (np.ndarray) --> complex dielectric constants
    """
    nList = np.array(nList)
    n = np.real(nList); k = np.imag(nList)
    eps = (n**2-k**2) + 1j*(2*n*k)
    return eps

def epsilon(w, eps_core, ne, tau, effMass):
    """
    returns a np.array with the dielectric constants accounting for
    free-carrier losses by using the Drude-Lorentz approximation
    for a np.ndarray of circular frequencies "w".

    Input:
        w (np.array) --> circular frequencies [rad/sec]
        eps_core (np.array or a constant) --> undoped dielectric constants
                                              for w. Elements are floats
                                              or complex numbers.
        ne --> free carrier density [cm-3]
        tau --> electron relaxation time [ps]
        effMass --> electron effective mass [SI units]

    Returns:
        eps (np.array) --> complex dielectric constants [adimensional]
    """
    from fundamentalConstants import e, eps0
    w = np.array(w)

    #conversion to SI units
    ne = ne * 1e6
    tau = tau * 1e-12

    eps = eps_core + (1j*ne * e**2 * tau)/(w * effMass * (1-1j*w*tau))/eps0
    return eps

def refIndexFromEps(eps):
    """
    Returns the complex refractive indices for dielectric constants
    given in the np.array "eps". It consideres a relative magnetic
    permeability equal to one.

    Input:
        eps (array-like) --> complex dielectric constants

    Returns:
        indices (2d np.array) --> complex refractive indices
            where:
                1st column: real parts.
                2nd column: imaginary parts
    """
    eps = np.array(eps)
    n = np.sqrt( (np.absolute(eps) + np.real(eps))/2 )
    k = np.imag(eps) / (2*n)
#    indices = np.zeros((len(eps),2))
#    indices[:,0] = n; indices[:,1] = k
#    return indices
    return n + 1j*k


def absorptConst(w, eps):
    """
    Returns the absorption coefficients for the circular frequencies
    "w" and corresponding complex dielectric constants "eps".

    Input:
        w (np.ndarray or list) --> circular frequencies [rad/s]
        eps (np.ndarray, type complex) --> dielectric constants

    Returns:
        absorption (np.ndarray, type float) --> absorption constants
                                                    [cm-1]
    """
    from fundamentalConstants import c
    w = np.array(w)
    refInd = refIndexFromEps(eps)
#    k = refInd[:,1]  #Imaginary part Ref. Index
    k = np.imag(refInd)
    absorption = 2* w * k / c
    return absorption * 1e-2

def ternaryInterpolate_linear(a, b, x):
    """
    Ternary AxB(1-x)C. Interpolates parameter P(ABC), where: a = P(AC), b = P(BC).
    Uses linear interpolation.
    """
    return a*x + b*(1-x)

def ternaryInterpolate_nonlinear(a, b, x):
    """
    Ternary AxB(1-x)C. Interpolates dielectric constant eps(ABC), where: a = eps(AC), 
    b = eps(BC). Uses non-linear interpolation.
    """
    r = x * (a-1)/(a-2) + (1-x)*(b-1)/(b-2)
    return (-2*r + 1) / (1-r)

class Material:
    """
    Material instances should contain the attributes: "f", "effMass", "n" or "fileN"
    (optionally "eps" or "fileEps"). These can be added with setter functions.
    """
    def __init__(self, manualMode=True):
        self.f = [] #frequencies of interest [Hz]
        self.n = [] #undoped refractive indices at frequencies of interest
        self.eps = [] #undoped dielectric constants at frequencies of interest
        self.effMass = None  #electron effective mass at band edge [Kg]
        self.fileN = None #Path to file containing undoped complex Ref. Indices.
                          #file contains three columns: freq [Hz], Re{n}, Im{n}
                          #Comment lines must start with symbol #
        self.fileEps = None #Path to file containing undoped complex Diel. Constants
                            #file contains three columns: freq [Hz], Re{eps}, Im{eps}
                            #Comment lines must start with symbol #
        self.manualMode = manualMode

    def set_f(self, f):
        """
        Sets freq. of interest (np.ndarray) to "f". If attribute "manualMode" is set to
        "True", attributes "n" and "eps" are initialized. If set to "False" refractive
        indices (or Diel. Const.) at "f" are automatically set from corresponding file.
        Input:
            f (iterator) --> frequencies of interest [Hz]
        Returns:
            None
        """
        self.f = np.array(f)
        print("Frequencies set.")
        if(self.manualMode):
            self.n = []
            self.eps= []
        else:
            if(self.fileN != None ): self.set_n_fromInterp()
            elif(self.fileEps != None ): self.set_eps_fromInterp()
            else: raise Exception("Set 'fileN' or 'fileEps' attribute first")
    def set_n(self, n):
        """
        Sets the undoped refractive indices for frequencies of interest. 
        At the same time sets "self.eps = []"
        Input:
            n (iterator) --> undoped complex refractive indices
                n[i] -> complex or float
        Returns:
            None
        """
        n = np.array(n)
        self.n = n; self.eps = []
        print("Core refractive indices set.")
    def set_eps(self, eps):
        """
        Sets the undoped dielectric constants for frequencies of interest
        At the same time sets "self.n = []"
        Input:
            eps (iterator) --> undoped complex dielectric constants
                eps[i] -> complex or float
        Returns:
            None
        """
        eps = np.array(eps)
        self.eps = eps; self.n = []
        print("Core dielectric constants set.")
    def set_n_fromInterp(self):
        """
        Sets the undoped refractive indices for frequencies of interest defined
        under attribute "f" using the method interpolateN. Assumes attributes
        "f" and "fileN" have been defined.
        Returns:
            None
        """
        print("interpolating ref. indices from file.")
        self.set_n( self.interpolate('n') )
    def set_eps_fromInterp(self):
        """
        Sets the undoped dielectric constants for frequencies of interest defined
        under attribute "f" using the method interpolateN. Assumes attributes
        "f" and "fileEps" have been defined.
        Returns:
            None
        """
        print("interpolating diel. constants from file.")
        self.set_eps( self.interpolate('eps') )
    def set_effMass(self, effMass):
        """
        Set the effective mass (float) in Kg for a given concentration of free
        carriers
        """
        self.effMass = effMass
        print("Effective mass set.")
    def set_file(self, File, param='n'):
        """
        Set instance attribute "fileN" or "fileEps" to "File" if param equals
        'n' or 'eps', respectively. Attributes "f", "n" and "eps" are initialized
        to empty lists. Guarantees attributes "fileN" and "fileEps" exists
        only one at a time.
        Input:
            File (string)
            param (string) --> can take value "n" or "eps"
        """
        param = param.lower()
        if(param=='n'): self.fileN = File; self.fileEps = None; print("Core ref. index file set")
        elif(param=='eps'): self.fileEps = File; self.fileN = None; print("Core diel. const. file set")
        else: raise Exception("param can only be set to 'n' or 'eps'")
        self.f = []; self.n = []; self.eps = []
    def set_manualMode(self, state):
        """
        Set instance attribute "manualMode"
        Input:
            state (boolean) --> if True, Ref. Indices must be entered manually, 
                                otherwise indices are interpolated from file 
                                self.fileN or self.fileEps.
        """
        self.manualMode = state
    def get_f(self):
        if(len(self.f)==0):
            raise Exception("frequencies not entered yet")
        return self.f
    def get_n(self):
        """
        returns undoped refractive indices
        """
        if(len(self.n)==0):
            if(len(self.eps)==0):
                raise Exception("no undoped refractive indices nor Diel. Const. entered yet")
            else:
                return refIndexFromEps(self.eps)
        return self.n
    def get_eps(self):
        """
        returns undoped dielectric constant
        """
        if(len(self.eps)==0):
            if(len(self.n)==0):
                raise Exception("no undoped dielectric constants nor Ref. Ind. entered yet")
            else:
                return epsFromRefInd(self.n)
        return self.eps
    def get_effMass(self):
        if(self.effMass==None):
            raise Exception("effective mass not entered")
        return self.effMass
    def get_fileN(self):
        if(self.fileN==None):
            raise Exception("file attribute for core ref. index not entered")
        return self.fileN
    def get_fileEps(self):
        if(self.fileEps==None):
            raise Exception("file attribute for core diel. constants not entered")
        return self.fileEps
    def get_file(self):
        """
        Returns tuple whose first element is attribute: fileN or fileEps, whichever is defined. 
        The second element being the string 'n' for FileN, 'eps' for fileEps. None otherwise.
        """
        if(self.fileN!=None): return (self.fileN, 'n')
        elif(self.fileEps!=None): return (self.fileEps, 'eps')
        else: return None
    def interpolate(self, param='n'):
        """
        returns the undoped complex refractive indices (param='n') or dielectric
        constans (param='eps') for the frequencies in attribute "f" using a
        linear interpolation on the indices in attribute "fileN" or "fileEps".
        Input:
            param (string) --> equal to 'n' or 'eps'. Tells variable of interest
        Returns:
            VI (np.ndarray) --> interpolated variable of interest
                VI[i] -> complex
        """
        from scipy.interpolate import interp1d
        param = param.lower()
        if(param=='n'): File = self.get_fileN()
        elif(param=='eps'): File = self.get_fileEps()
        else: raise Exception("param can only be set to 'n' or 'eps'")
        f = self.get_f()
        freq, Real, Imag = np.loadtxt(File, unpack=True)
        funcR = interp1d(freq, Real)
        funcI = interp1d(freq, Imag)
        #Get interpolations
        interpR = funcR(f); interpI = funcI(f)
        VI = interpR + 1j*interpI
        return VI

#    def er_core(self):
#        """
#        Returns np.ndarray with the undoped complex dielectric
#        constants for the refractive indices in attribute "n".
#        """
#        eps_core = epsFromRefInd( self.get_n() )
#        return eps_core
#
#    def refIndex_core(self):
#        """
#        Returns a float or np.ndarray with the undoped complex refractive
#        indices for the dielectric constants in attribute "eps".
#        """
#        return refIndexFromEps( self.get_eps() )

    def epsilon(self, ne, tau):
        """
        Returns the final dielectric constant.
        Input:
            ne --> free carrier density [cm-3]
            tau --> electron relaxation time [ps]
        Returns:
            eps --> complex dielectric constants 
        """
        eps_core = self.get_eps()
        f = self.get_f()
        effMass = self.get_effMass()
        w = 2 * np.pi * f
        eps = epsilon(w, eps_core, ne, tau, effMass)
        return eps

    def refracIndex(self, ne, tau):
        """
        Returns the final refractive indices.
        Input:
            ne --> free carrier density [cm-3]
            tau --> electron relaxation time [ps]
        Returns:
            refIndex --> complex refractive indices
        """
#        eps_core = self.get_eps()
#        f = self.get_f()
#        effMass = self.get_effMass()
#        w = 2 * np.pi * f
#        eps = epsilon(w, eps_core, ne, tau, effMass)
        eps = self.epsilon(ne, tau)
        return refIndexFromEps(eps)

    def absorption(self, ne, tau):
        """
        Returns absorption coefficients for linear frequencies in
        attribute "f".
        Input:
            ne --> free carrier density [cm-3]
            tau --> electron relaxation time [ps]

        Returns:
            absorption (np.ndarray) --> absorption constants [cm-1]
                absorption[i] -> float
        """
        if(self.fileEps != None): eps_core = self.get_eps()
        else: eps_core = self.er_core()
        f = self.get_f()
        effMass = self.get_effMass()
        w = 2 * np.pi * f
        eps = epsilon(w, eps_core, ne, tau, effMass)
        return absorptConst(w, eps)

class Vacuum(Material):
    """
    Returns basic parameters of "Vacuum"
    """
    def __init__(self, File=os.path.join(currentDir,"refIndexTables/Vacuum.dat"), param='n', manualMode=False):
        from fundamentalConstants import me
        self.f = []
        self.n = []
        self.eps = []
        Material.set_effMass(self, me)
        self.fileN = None; self.fileEps = None
        Material.set_file(self, File, param)
        self.manualMode = manualMode


class InP(Material):
    """
    Returns basic parameters of "InP"
    """
    def __init__(self, File=os.path.join(currentDir,"refIndexTables/InP_Adachi.dat"), param='n', manualMode=False):
        from fundamentalConstants import me
        self.f = []
        self.n = []
        self.eps = []
#        self.effMass=0.073*me
        Material.set_effMass(self, 0.073*me)   #Taken from Semiconductor: Data Handbook 3rd Ed., Otfried Madelung
#        self.fileN = directory+"/InP.dat"
        self.fileN = None; self.fileEps = None
        Material.set_file(self, File, param)
        self.manualMode = manualMode


class Au(Material):
    """
    Returns basic parameters of "Au"
    """
    def __init__(self, File=os.path.join(currentDir,"refIndexTables/AuCombined.dat"), param='n', manualMode=False):
        from fundamentalConstants import me
        self.f = []
        self.n = []
        self.eps = []
        Material.set_effMass(self, 1.1*me)
        self.fileN = None; self.fileEps = None
        Material.set_file(self, File, param)
        self.manualMode = manualMode

class GaAs(Material):
    """
    Returns basic parameters of "GaAs"
    """
    def __init__(self, File=os.path.join(currentDir,"refIndexTables/GaAs.dat"), param='n', manualMode=False):
        from fundamentalConstants import me
        self.f = []
        self.n = []
        self.eps = []
        Material.set_effMass(self, 0.0635*me)   #Vurgaftman et. al., Appl. Phys. Rev. 89 (2001) (Band Parameters...)
        self.fileN = None; self.fileEps = None
        Material.set_file(self, File, param)
        self.manualMode = manualMode

class InAs(Material):
    """
    Returns basic parameters of "InAs"
    """
    def __init__(self, File=os.path.join(currentDir,"refIndexTables/InAs.dat"), param='n', manualMode=False):
        from fundamentalConstants import me
        self.f = []
        self.n = []
        self.eps = []
        Material.set_effMass(self, 0.022*me)   #Vurgaftman et. al., Appl. Phys. Rev. 89 (2001) (Band Parameters...)
        self.fileN = None; self.fileEps = None
        Material.set_file(self, File, param)
        self.manualMode = manualMode

class AlAs(Material):
    """
    Returns basic parameters of "AlAs"
    """
    def __init__(self, File=os.path.join(currentDir,"refIndexTables/AlAs.dat"), param='n', manualMode=False):
        from fundamentalConstants import me
        self.f = []
        self.n = []
        self.eps = []
        Material.set_effMass(self, 0.124*me)   #Adachi, Properties of semiconductor alloys...(Pg. 230)
        self.fileN = None; self.fileEps = None
        Material.set_file(self, File, param)
        self.manualMode = manualMode

class AlInAs(Material):
    """
    Returns basic parameters of "Al(0.48)In(0.52)As"
    """
    def __init__(self, File=os.path.join(currentDir,"refIndexTables/AlInAs.dat"), param='n', manualMode=False):
        from fundamentalConstants import me
        self.f = []
        self.n = []
        self.eps = []
        Material.set_effMass(self, 0.078*me) #Laser action tuning osc. strength, Faist, Nat. 1997
        self.fileN = None; self.fileEps = None
        Material.set_file(self, File, param)
        self.manualMode = manualMode

class GaInAs(Material):
    """
    Returns basic parameters of "Ga(0.47)In(0.53)As"
    """
    def __init__(self, File=os.path.join(currentDir,"refIndexTables/GaInAs.dat"), param='n', manualMode=False):
        from fundamentalConstants import me
        self.f = []
        self.n = []
        self.eps = []
        Material.set_effMass(self, 0.043*me) #Laser action tuning osc. strength, Faist, Nat. 1997
        self.fileN = None; self.fileEps = None
        Material.set_file(self, File, param)
        self.manualMode = manualMode

    def refIndex_transparentRegion(self):
#        def func(x):
#            return x**-2 * (2-np.sqrt(1+x)-np.sqrt(1-x))
#        import parametersAdachi as PA
#        from fundamentalConstants import hev
#        param =  PA.parametersInGaAs(0.53)
#        f = self.get_f() ; w = 2*np.pi*f; E=hev*w
#        A, B, E0, Delta0 = param
#        x0 = E / E0; xs0 = E / (E0 + Delta0) 
#        nSquared = A * (func(x0) + 0.5*func(xs0)*(E0/(E0+Delta0))**(1.5)) + B
#        return np.sqrt(nSquared)
        return PA.refIndex_transparentRegion('InGaAs', 0.53, self.get_f())

    def makePlotRefIndex(self):
        from fundamentalConstants import hev
        PA.makePlotRefIndex('InGaAs', 0.53, self.get_f()*(2*np.pi)*hev, 'InGaAs')
