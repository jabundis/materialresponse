import numpy as np

h = 1.0545718e-34  #hbar in SI
hev = 6.582119514e-16 #hbar in eV
e = 1.6021766208e-19  #fundamental charge in C
me = 9.10938356e-31 #electron mass in Kg
c = 2.99792458e8 #vacuum speed of light in m/s (exact)
mu0 = 4*np.pi*1e-7 #vacuum permeability in N/A^2 (exact)
eps0 = 1/(mu0 * c**2) #Vacuum permittivity in F/m (exact)
