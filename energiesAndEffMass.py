import numpy as np
import matplotlib.pyplot as plt

#Materials:
materials = ('In','Ga','Al', 'As','P','Sb')

#Dictionaries with material parameters

#Adachi Parameters
#Format 'Values' index: { material: (A , B), ... }
strengthParam = {'Indices':{'A':0, 'B':1},
                 'Values':{'AlP':(18.39,-0.74), 'InAs':(5.14,10.15), 'GaAs':(6.30,9.40), 
                           'AlAs':(25.30,-0.80),'InP':(8.40, 6.60), 'GaP':(14.71,4.38),
                           'AlSb':(59.68,-9.53)},
                 'BowingParam':{}
                 }

#Basic Band Parameters
#Format 'Values' index: { material: (E0, Delta0, EgX, EgL), ... } in eV at T=0K
#Note:
#To calculate E0 for T!=0K use appropriate function.
energies = {'Indices':{'E0':0, 'Delta0':1, 'EgX':2, 'EgL':3},
            'Values':{'AlP':(3.58,0.10,2.48,3.30), 'AlAs':(3.099,0.28,2.24,2.46),
                      'GaAs':(1.519,0.341,1.981,1.815),'InP':(1.4236,0.108,2.384,2.014),
                      'InAs':(0.417,0.39,1.433,1.133),'GaP':(2.74,0.10,2.26,2.63),
                      'AlSb':(2.30,0.72,1.61,1.21)}, 
            'BowingParam':{'InGaAs':(0.477, 0.15, 1.4, 0.33), 'AlGaAs':(np.NaN,0,0.055,0),
                           'AlInAs':(0.70, 0.15, 0, 0)},
            'Source':''
            }

#More Band Parameters
#Format 'Values' index: { material: (Ep, F, VBO), ... }
# Ep, VBO -> eV,  F -> Adimensional
#
#Notes:
# Ep, F -> Temperature independent
band = {'Indices':{'Ep':0, 'F':1, 'VBO':2},
        'Values':{'InP':(20.7,-1.31,-0.94), 'InAs':(21.5,-2.90,-0.59),
                  'GaAs':(28.8,-1.94,-0.80), 'AlAs':(21.1,-0.48,-1.33)},
        'BowingParam':{'InGaAs':(-1.48, 1.77, -0.38), 'AlGaAs':(0,0,0),
                       'AlInAs':(-4.81, -4.44, -0.64)},
        'Source':''
        }

#Parameters for temperature dependence of E0
#Note:
#It is not good to interpolate these values for ternaries
#(hence the lack of the key "BowingParam")
E0Temp = {'Indices':{'alpha':0, 'beta':1},
          'Values':{'AlP':(0.5771e-3,372), 'AlAs':(0.885e-3,530),
                    'GaAs':(0.5405e-3,204),'InP':(0.363e-3,162),
                    'InAs':(0.276e-3,93),'GaP':(np.NaN,np.NaN),
                    'AlSb':(0.42e-3,140)}, 
          'Source':''
          }

#Bowing parameters for the effective mass in m0 units at the Gamma point
BowingEffMass = {'InGaAs':0.0091, 'AlGaAs':0, 'AlInAs':0.049} 

#All properties
dictionaries = [strengthParam, energies, band, E0Temp]


def findElements(alloy, materials=materials):
    """
    Finds atoms used in alloy that are contained in the list "materials".
    The elements are extracted from left to right according to "alloy"
    and are capitalized. If no atoms are found returns an Exception.
    
    Input:
        alloy (string) -> alloy
        materials (list or tuple) -> Set of possible atoms

    Output:
        elements (list) -> Atoms that comprise alloy
    """
    materials = [materials[i].lower() for i in range(len(materials))]
    elements = []
    ind = 0
    n = len(alloy)
    while(ind < n):
        if( alloy[ind:ind+2].lower() in materials ):
            elem = alloy[ind:ind+2].capitalize()
            elements.append(elem)
            ind += 2
        elif( alloy[ind:ind+1].lower() in materials ):
            elements.append( alloy[ind:ind+1].capitalize() )
            ind += 1
        else:
            raise Exception('At least one material is not found in materials tuple. Check your Alloy')
    return elements


def binary(elem1, elem2,dictionary):
    """
    Returns string with binary formed by the two elements "elem1" and "elem2" in the
    order the binary appears in "dictionary". If binary is not contained in dictionary, 
    an empty string is returned.
    """
    element = ""
    if( elem1+elem2 in dictionary['Values'] ): element = elem1+elem2
    elif( elem2+elem1 in dictionary['Values'] ): element = elem2+elem1
    return element


def binariesFromTernary(ternary, dictionary, materials=materials):
    """
    Returns list with the two binaries contained in "ternary" following the order of
    the "ternary" (i.e. "GaInAs" -> ["GaAs", "InAs"]), as long as these two appear 
    in "dictionary". Moreover, each final binary appears exactly as in "dictionary".
    Throws an exception if ternary cannot be formed from binaries in dictionary.

    Input:
        "ternary" -> string
        "dictionary" -> dictionary containing parameters

    Returns:
        binaries -> list with two strings (the binaries)
    """

    elements = findElements(ternary, materials)
    assert len(elements)==3, "alloy should be ternary"
    binaries = []; 
    for i in range(2):
        if( len( binary(elements[i],elements[2],dictionary) )!=0 ): 
            binaries.append( binary(elements[i],elements[2],dictionary) )
        else:
            raise Exception("Binary {} was not found in dictionary".format(elements[i]+elements[2]))
    return binaries


def findParam(param, dictionaries):
    """
    Looks for "param" in "dictionaries". If it finds a match, it returns a tuple with
    the index of sought parameter and the dictionary where it was found. Otherwise 
    throws an Exception.
    """
    for dictionary in dictionaries:
        if(param in dictionary['Indices']):
            ind = dictionary['Indices'][param] #Index of sought parameter
            return (ind, dictionary)
    raise Exception("parameter sought not found in dictionaries")


def bowing(param, ternary, x=None, dictionary=None, dictionaries=dictionaries):
    """
    x is only required for ternary "AlGaAs"
    """
    elements = findElements(ternary)
    assert len(elements)==3, "material should be a ternary"
    if(set(elements)==set(['Al','Ga','As'])): return -0.127 + 1.310*x
    #Elements in ternary are capitalized
    ternary = ''
    for elem in elements: 
        ternary += elem        
    #Find dictionary with the sought parameter and its index
    if(dictionary==None):
        dictionaryList = dictionaries
    else:
        dictionaryList = [dictionary]
    ind, dictionary = findParam(param, dictionaryList)
    #Look for ternary in dictionary that contains 'param'
    if(ternary not in dictionary['BowingParam']):
        ternary = elements[1] + elements[0] + elements[2] #Switch first two elements
        if(ternary not in dictionary['BowingParam']):
            print('Bowing Parameter not found.' )
            return None
    return dictionary['BowingParam'][ternary][ind]


def parabolicApprox(alloy, x, param='E0', dictionary=energies):
    """
    Finds parabolic approximation of "param" in "dictionary" for "alloy".
    Alloy is a ternary given as a string: 'ABC'. It's assumed AxB(1-x)C. 
    Considers bowing parameter. If Bowing is not known, a linear approximation
    is used. It throws an exception if alloy is not found in dictionary.
    """
    elements = findElements(alloy)
    binaries = binariesFromTernary(alloy, dictionary) #Throws exception if alloy is not found
    ind = dictionary['Indices'][param] #Index of sought parameter
    alloy = ''
    for elem in elements: 
        alloy += elem        #Elements in Alloy are capitalized
    if(alloy not in dictionary['BowingParam']):
        alloy = elements[1] + elements[0] + elements[2] #Switch first two elements
        if(alloy not in dictionary['BowingParam']):
            print('Bowing Parameter not found. Linear Approx. is used.' )
            alloy = elements[0] + elements[1] + elements[2] #Reestablish order of elements in alloy
            return linearApprox(alloy, x, param, dictionary)
    approximation = ( x*dictionary['Values'][binaries[0]][ind] +  
                      (1-x)*dictionary['Values'][binaries[1]][ind] -   
                      x*(1-x)*dictionary['BowingParam'][alloy][ind] )
    return approximation


def linearApprox(alloy, x, param='E0', dictionary=energies):
    """
    Finds linear approximation of "param" in "dictionary" for "alloy". Alloy
    is a ternary given as a string: 'ABC'. It's assumed AxB(1-x)C. 
    It throws an exception if alloy is not found in dictionary.
    """
    elements = findElements(alloy)
    binaries = binariesFromTernary(alloy, dictionary) #Throws exception if alloy is not found
    ind = dictionary['Indices'][param] #Index of sought parameter
    approximation = ( x*dictionary['Values'][binaries[0]][ind] +  
                      (1-x)*dictionary['Values'][binaries[1]][ind] )
    return approximation

def parameter(param, material, x=None, dictionary=None, dictionaries=dictionaries):
    """
    Returns parameter "param" for a binary or ternary alloy entered as "material", 
    using "dictionary" or "dictionaries" (interpolation is used in case of a ternary). 
    If a ternary is specified, "x" represents the percent of the first binary. 
    "x" is not required (ignored) for a binary. Optionally, the "dictionary" containing 
    "param" can be specified, or alternatively the list of dictionaries to look for it.
    If "material" is a ternary, a parabolic approximation will be performed if Bowing 
    is present, a linear approximation is carried out otherwise.

    Input:
        param (string) -> parameter to be returned
        material (string) -> binary or ternary alloy
        x (float in [0,1]) -> percent of first binary in case of ternary.
                              Ignored in case of binary.
        dictionary -> optional. Dictionary to look for "param"
        dictionaries (array-like) -> optional. Sequence of dictionaries used
                                     to perform search. Not considered if 
                                     "dictionary" is provided. 
    Returns:
        parameter (float) 
    """
    def paramFromBinary(elem1, elem2, ind, dictionary):
        material = binary(elem1,elem2,dictionary)
        if(len(material)!=0): return dictionary['Values'][material][ind]
        else: raise Exception("Binary not found")

    elements = findElements(material)
    if(len(elements)==2):
        if(dictionary==None):
            dictionaryList = dictionaries
        else:
            dictionaryList = [dictionary]
        ind, dictionary = findParam(param, dictionaryList)
        return paramFromBinary(elements[0], elements[1], ind, dictionary)
    if(len(elements)==3):
        if(dictionary==None):
            ind, dictionary = findParam(param, dictionaries)
        return parabolicApprox(material, x, param, dictionary)


def getParam(param, material, x=None):
    return parameter(param, material, x, None, dictionaries)

def getE0(material, x=None, T=300):
    def parabInterp(par1, par2, bowing, x):
        return x*par1 + (1-x)*par2 - x*(1-x)*bowing

    def E0binary(binary, T):
#        elements=findElements(binary)
#        if( set(elements)==set(['Ga','P']) ):
#            raise Exception('GaP Bandgap needs to be implemented.')
        E0_T0 = parameter('E0', binary)
        alpha = parameter('alpha', binary) 
        beta  = parameter('beta', binary)
        return E0_T0 - (alpha*T**2) / (T+beta)

    elements = findElements(material)
    if(len(elements)==2):
        return E0binary(material, T)
    elif(len(elements)==3):
        binary1 = elements[0] + elements[2]
        binary2 = elements[1] + elements[2]
        E01 = E0binary(binary1, T)
        E02 = E0binary(binary2, T)
        Bowing = bowing('E0', material)
        return parabInterp(E01, E02, Bowing, x)

def getEffMass(material, x=None, T=300):
    """
    Effective mass in m0 units
    """
    F = parameter('F', material, x)
    Ep = parameter('Ep', material, x) 
    E0 = getE0(material, x, T)
    Delta0 = parameter('Delta0', material, x)
    return ( 1 + 2*F + (Ep*(E0+2*Delta0/3.0)) / (E0*(E0+Delta0)) )**-1

